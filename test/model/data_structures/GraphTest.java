package model.data_structures;

import junit.framework.TestCase;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Iterator;

import junit.framework.TestCase;



public class GraphTest extends TestCase{

	//Se crea un nuevo grafo
	private Graph<Long, String, String> grafo = new Graph<>();


	public void escenario1() {

		grafo.addVertex((long) 123, "114.5|120.7");

	}

	public void escenario2() {

		grafo.addVertex((long)123, "114.5|120.7");
		grafo.addVertex((long)222, "118.5|120.7");


		grafo.addEdge((long)123, (long)222, "200");


	}


	public void escenario3() {

		grafo.addVertex((long)123, "hola");


	}
	
	public void escenario5() {
		
		grafo.addVertex((long)123, "hola");
		grafo.addVertex((long)222, "hola2");
		
		grafo.addEdge((long)123, (long)222, "hi");
		
	}
	
	
	public void escenario7() {
		
		grafo.addVertex((long)123, "hola");
		grafo.addVertex((long)222, "hi");
		grafo.addVertex((long)223, "hi");
		
		Vertice v = grafo.getVertices().get((long)123);
		Vertice v2 = grafo.getVertices().get((long)222);
		
		Arco arco = new Arco(v.getId(), v2.getId(), 222.87);
		
		v.addEdge(arco);
		v.addAdj(v2.getId());
		
		
	}


	public void test1() {

		escenario1();
		assertEquals(1, grafo.getNumVertices());

	}


	public void test2() {

		escenario2();

		assertEquals(1, grafo.getNumArcos());

		Bag arcos = grafo.getVertices().get((long)123).getArcos();

		Iterator<Arco> it = arcos.iterator();
		Arco actual = new Arco<Integer, String>(1, 1, "2");

		boolean termino = false;

		while(it.hasNext() && !termino) {

			actual = it.next();

			if(actual.getV1().equals(123)) {

				termino = true;

			}

		}

		assertEquals("200", actual.getInfo());

	}

	public void test3() {

		escenario3();

		Vertice v = grafo.getVertices().get((long)123);
		assertEquals("hola", v.getInfo());

	}
	
	
	public void test4() {
		
		escenario3();
		
		Vertice v = grafo.getVertices().get((long)123);
		assertEquals("hola", v.getInfo());
		
		grafo.setInfoVertex((long)123, "hola2");
		assertEquals("hola2", v.getInfo());
		
		
	}
	
	
	public void test5() {
		
		escenario5();
		
		Bag arcos = grafo.getVertices().get((long)123).getArcos();

		Iterator<Arco> it = arcos.iterator();
		Arco actual = new Arco<Integer, String>(1, 1, "2");

		boolean termino = false;

		while(it.hasNext() && !termino) {

			actual = it.next();

			if(actual.getV1().equals(123)) {

				termino = true;

			}

		}

		assertEquals("hi", actual.getInfo());	
		
	}
	
	
	public void test6() {
		
		escenario5();
		Bag arcos = grafo.getVertices().get((long)123).getArcos();

		Iterator<Arco> it = arcos.iterator();
		Arco actual = new Arco<Integer, String>(1, 1, "2");

		boolean termino = false;

		while(it.hasNext() && !termino) {

			actual = it.next();

			if(actual.getV1().equals(123)) {

				termino = true;

			}

		}

		assertEquals("hi", actual.getInfo());
		
		actual.setInfo("hola");
		
		assertEquals("hola", actual.getInfo());
		
	}
	
	
	
	public void test7() {
		
		escenario7();
		
		int i = 0;
		Vertice v = grafo.getVertices().get((long)123);
		
		Iterator<Vertice> it = v.getAdj().iterator();
		
		while(it.hasNext()) {
			
			it.next();
			
			i++;
			
		}
		
		assertEquals(1, i);
		
	}
	

}
