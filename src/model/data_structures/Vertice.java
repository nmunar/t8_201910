package model.data_structures;

public class Vertice<K, V, A> {

	/**
	 * Lista de arcos que dispone el v�rtice
	 */
	private Bag<Arco> arcos;


	/**
	 * Lista de v�rtices adyacentes a este
	 */
	private Bag<K> adj;

	/**
	 * Identificador �nico del v�rtice
	 */
	private K id;


	/**
	 * Indicador de si ya fue recorrido
	 */
	private boolean marca;

	/**
	 * Informaci�n de un v�rtice
	 */
	private V info;

	/**
	 * Latitud del v�rtice
	 */
	private V latitud;

	/**
	 * Latitud del v�rtice
	 */
	private V longitud;


	public Vertice(K pId, V infoVertice) {

		info = infoVertice;
		arcos = new Bag<Arco>();
		adj = new Bag<K>();
		id = pId;
		marca = false;

	}


	public Bag<Arco> getArcos() {

		return arcos;

	}

	public boolean hayArcos()
	{
		boolean hay = false;
		if(getArcos().isEmpty())
		{
			hay= false;
		}else
		{
			hay = true;
		}

		return hay;
	}

	public Bag<K> getAdj() {

		return adj;

	}


	public void addEdge(Arco pArco) {

		arcos.add(pArco);

	}


	public void addAdj(K pAdj) {

		adj.add(pAdj);

	}


	public V getInfo() {

		return info;

	}

	@SuppressWarnings("unchecked")
	public void setInfoJunta()
	{
		info = (V)(getLatitud()+"|"+getLongitud());
	}

	public V getLatitud() {

		return latitud;

	}

	public void setLat(V lat)
	{
		latitud = lat;
	}

	public V getLongitud() {

		return longitud;

	}

	public void setLon(V lon) 
	{
		longitud = lon;
	}

	public K getId() {

		return id;

	}

	public void setId(K pId)
	{
		id=pId;
	}

	public boolean getMarca() {

		return marca;

	}


	public void setInfo(V pInfo) {

		info = pInfo;

	}	


}
