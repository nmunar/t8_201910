package model.data_structures;

/**
 * Undirected, unweighted simple graph data type
 * 
 *  Notes:
 *  
 *    Parallel edges are not allowed
 *    Self loops are allowed
 *
 *  This Graph class was adapted from 
 *  http://www.cs.princeton.edu/introcs/45graph/Graph.java by 
 *  by Robert Sedgewick and Kevin Wayne
 */

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Graph<K, V, A>{


	/**
	 * Lista de v�rtices del grafo
	 */
	private hashTableLinearProbing<K, Vertice> vertices;

	/**
	 * Cantidad de v�rtices
	 */
	private int numVertices;

	/**
	 * Cantidad de arcos en el grafo
	 */
	private int numArcos;
	
	private Bag<K> idVertices = new Bag<K>();;


	/**
	 * Se crea un grafo nuevo no dirigido con 0 v�rtices y arcos
	 */
	public Graph() {

		vertices = new hashTableLinearProbing<K, Vertice>();
		numVertices = 0;
		numArcos = 0;

	}
	
	
	
	public hashTableLinearProbing<K, Vertice> getVertices() {
		
		return vertices;
		
	}
	
	
	public int getNumVertices() {
		
		return numVertices;
		
	}
	
	
	public int getNumArcos() {
		
		return numArcos;
		
	}



	public void addVertex(K idVertex, V infoVertex) {

		Vertice newVertice;
		newVertice = vertices.get(idVertex);

		if(newVertice == null) {

			newVertice = new Vertice(idVertex, infoVertex);
			vertices.put(idVertex, newVertice);
			idVertices.add(idVertex);
			numVertices++;

		}

	}
	
	public void deleteVertex(K key)
	{
		getVertices().delete(key);
		numVertices--;
	}
	
	public Bag<K> idVertices()
	{
		return idVertices;
	}


	public void addEdge(K idVertexIni, K idVertexFin, A infoArc ) {

		Vertice v1 = vertices.get(idVertexIni);
		Vertice v2 = vertices.get(idVertexFin);
		
		Arco arco = new Arco(idVertexIni, idVertexFin, infoArc);

		if(v1 != null && v2 != null) {

			v1.addEdge(arco);
			v2.addEdge(arco);
			
			v1.addAdj(v2.getId());
			v2.addAdj(v1.getId());

			numArcos++;

		}

	}


	public V getInfoVertex(K id) {

		Vertice v = vertices.get(id);
		V info = null;

		if(v != null) {

			info =(V) v.getInfo();

		}
		
		return info;
	}
	
	
	public void setInfoVertex(K idVertex, V infoVertex) {
		
		Vertice v = vertices.get(idVertex);
		v.setInfo(infoVertex);

	}
	
	
	public void setInfoArc(K idVertexIni, K idVertexFin, A infoArc) {
		
		Vertice v = vertices.get(idVertexIni);
		Bag<Arco> arcos = v.getArcos();
		
		Iterator<Arco> it = arcos.iterator();
		
		boolean encontrado = false;
		while(it.hasNext() && !encontrado) {
			
			Arco actual = it.next();
			
			if(actual.getV1().equals(idVertexIni) && actual.getV2().equals(idVertexFin)) {
				
				encontrado = true;
				actual.setInfo(infoArc);
				
			}
			
		}
		
	}
	
	
	
	public Iterator<K> adj(K idVertex) {
		
		Vertice v = vertices.get(idVertex);
		
		return v.getAdj().iterator();
		
	}
	
	
	

}
