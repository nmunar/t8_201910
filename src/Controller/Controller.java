package Controller;

import java.awt.List;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.json.simple.JSONArray;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import com.sun.corba.se.impl.oa.poa.ActiveObjectMap.Key;

import jdk.internal.org.objectweb.asm.tree.analysis.Value;
import model.data_structures.*;

@SuppressWarnings("unused")
public class Controller {

	boolean carga = false;

	private Graph<String, String, Double> grafo;
	private Graph<String, String, Double> grafo2;

	private boolean condic = false;

	private FormatReader handler;
	private JSONArray vertex;
	private JSONArray edge;


	Mapa tamp;

	public Controller() {

	}

	public void menu()
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 8----------------------");
		System.out.println("1. Cargar grafo");
		System.out.println("2. Mostrar grafo");
		System.out.println("3. Salir");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");
	}

	/**
	 * Leer los datos del los vertices de los archivos.
	 * Todas infracciones (MovingViolation) deben almacenarse en una Estructura de Datos (en el mismo orden como estan los archivos)
	 * @return numero de infracciones leidas 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 */
	public void cargarGrafo(int i) throws ParserConfigurationException, SAXException, IOException 
	{
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		SAXParser saxParser = saxParserFactory.newSAXParser();

		if(i==1)
		{
			File file = new File("data\\Central-WashingtonDC-OpenStreetMap.xml");
			handler = new FormatReader();
			saxParser.parse(file, handler);
			System.out.println();
			carga = true;

			if(condic == false) {
				grafo = handler.getGrafo();
				vertex = handler.getVertexArray();
				edge = handler.getEdgesArray();
				handler.writeJson(vertex, edge);
			} else {

				grafo = handler.readJson();

			}
		}else
		{
			File file2 = new File("data\\map.xml");
			handler = new FormatReader();
			saxParser.parse(file2, handler);
			System.out.println();
			carga = true;


			if(condic == false) {
				grafo2 = handler.getGrafo();
				vertex = handler.getVertexArray();
				edge = handler.getEdgesArray();
				handler.writeJson(vertex, edge);
			} else {

				grafo2 = handler.readJson();

			}
		}

	}



	//	public void cargaOpcional() throws ParserConfigurationException, SAXException, IOException{
	//
	//		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
	//		SAXParser saxParser = saxParserFactory.newSAXParser();
	//		File file = new File("data\\Central-WashingtonDC-OpenStreetMap.xml");
	//		//File file2 = new File("data\\map.xml");
	//		handler = new FormatReader();
	//		FormatReader handler2 = new FormatReader();
	//
	//		saxParser.parse(file, handler);
	//		carga = true;
	//
	//	}

	public void eliminarVerticesSolos()
	{	int cantvertices = grafo.getNumVertices();
	Iterator<String> it= idVertices().iterator();
	Graph adj = grafo;

	while(it.hasNext())
	{
		String key = it.next();
		if(((Vertice) adj.getVertices().get(key)).hayArcos()==false)
		{
			cantvertices--;
			//adj.deleteVertex(key);
		}
	}

	System.out.println(cantvertices);

	}

	public Bag<String> idVertices()
	{
		return grafo.idVertices();

	}



	/**
	 * Reportar el número de vértices intersección y el número de arcos del grafo inicial construido en el
		documento Leame.txt. 
	 */
	public void reportarVA() {

		System.out.println("Cantidad de vértices: " + grafo.getNumVertices());
		System.out.println("Cantidad de arcos: " + grafo.getNumArcos());

	}



	/**

	 * M�todo run

	 * @throws IOException

	 * @throws SAXException

	 * @throws ParserConfigurationException

	 */

	public void run() throws ParserConfigurationException, SAXException, IOException {



		Scanner sc = new Scanner(System.in);

		boolean fin = false;



		while(!fin)

		{

			menu();



			int option = sc.nextInt();

			switch(option)

			{

			case 1:



				if(carga == true) {



					System.out.println("Los datos ya se habian cargado.");



				} else {





					System.out.println("---------ISIS 1206 - Estructuras de datos----------");

					System.out.println("---------------------Taller 8----------------------");

					System.out.println("1. Cargar datos Washington DC");

					System.out.println("2. Cargar datos del mapa completo");

					System.out.println();



					Scanner sc1 = new Scanner(System.in);



					int option2 = sc1.nextInt();

					switch(option2)

					{

					case 1:

						File file = new File(".\\data\\persistencia.txt");



						if(!file.exists())

						{

							this.cargarGrafo(1);

							System.out.println("\nVertices Washington: " + grafo.getNumVertices());

							System.out.print("Correccion de vertices con adyacentes: ");

							eliminarVerticesSolos();

							System.out.println("Arcos Washington: " + grafo.getNumArcos());

							System.out.println();

						}else

						{



							System.out.println("¿Desea cargar un nuevo grafo o cargar el anterior?");

							System.out.println("Digite '1' para uno nuevo, '2' para el anterior");



							int resp = sc.nextInt();



							if(resp == 1) {



								//System.out.println("Cargar grafo 1 o 2");         

								this.cargarGrafo(1);

								System.out.println("\nVertices Washington: " + grafo.getNumVertices());

								System.out.println("Arcos Washington: " + grafo.getNumArcos());
								
								System.out.print("Correccion de vertices con adyacentes: ");

								eliminarVerticesSolos();

								System.out.println();



							} else if(resp == 2) {                        



								condic = true;

								cargarGrafo(1);



								System.out.println("\nVertices Washington: " + grafo.getNumVertices());

								System.out.println("Arcos Washington: " + grafo.getNumArcos());
								
								System.out.print("Correccion de vertices con adyacentes: ");

								eliminarVerticesSolos();

								System.out.println();



							}





						}



						break;

					case 2:

						File file2 = new File(".\\data\\persistencia.txt");



						if(!file2.exists())

						{

							this.cargarGrafo(2);

							System.out.println("Vertices map: "+grafo2.getNumVertices());

							System.out.println("Arcos map: "+grafo2.getNumArcos());

							System.out.println();

						}else {



							System.out.println("¿Desea cargar un nuevo grafo o cargar el anterior?");

							System.out.println("Digite '1' para uno nuevo, '2' para el anterior");


							int resp = sc.nextInt();


							if(resp == 1) {


								this.cargarGrafo(2);

								System.out.println("\nVertices Washington: " + grafo2.getNumVertices());

								System.out.println("Arcos Washington: " + grafo2.getNumArcos());

								System.out.println();


							} else if(resp == 2) {                        

								condic = true;

								cargarGrafo(2);


								System.out.println("\nVertices Washington: " + grafo2.getNumVertices());

								System.out.println("Arcos Washington: " + grafo2.getNumArcos());

								System.out.println();


							}


						}

						break;

					}

				}


				carga = true;

				break;


			case 2:

				if(carga != true) {


					System.out.println("Los datos no se han cargado para versualizar el grafo");

				} else {

					tamp = new Mapa("Grafo Cargado", grafo);

				}

				break;


			case 3: 

				fin=true;

				sc.close();



				break;

			}

		}

	}





}
